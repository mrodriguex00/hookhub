﻿using System;
using System.Net;
using System.Net.Sockets;

namespace HookHub.Core.Models
{
    public class HookConnection
    {
        private string _hookName;
        private string _connectionID;

        public string HookName
        {
            get
            {
                _hookName ??= "";
                return (_hookName);
            }
            set { _hookName = value; }
        }

        public string ConnectionID
        {
            get
            {
                _connectionID ??= "";
                return (_connectionID);
            }
            set { _connectionID = value; }
        }

    }
}
